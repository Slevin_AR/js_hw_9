const createTag = (tagName, className, text) => {
    const tag = document.createElement(tagName);
    tag.classList.add(className);
    tag.textContent = text;
    return tag;
}
// className можно ли передать как масив ? 
export default createTag;