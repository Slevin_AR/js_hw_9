import createTag from "./function.js";  //// className можно ли передать как масив ? 

const body = document.querySelector('body');
const topHeader = ["div", "main-head", "HEADER"];
const topHeaderNav = ["div", "main-head-nav", "NAV"];
const mainBlock = ["div", "main-block-middle", ""];
const leftSection = ["div", "left-section", "SECTION"];
const rightSection = ["div", "right-section", "SECTION"];
const righSectionHeader = ["div", "righ-section-header", "HEADER"];
const righSectionNav = ["div", "righ-section-nav", "NAV"];
const leftSectionMainHeader = ["div", "left-section-main-header", "HEADER"];

const topArticle = ["div", "top-article", "ARTICAL"];
const topArticleHeader = ["div", "top-article-header", "HEADER"];
const topArticleMainPar = ["div", "top-article-main-par", "P"];
const topArticleSub = ["div", "top-article-sub", ""];
const topArticleSubPar = ["div", "top-article-sub-par", "P"];
const topArticleSubAside = ["div", "top-article-sub-aside", "ASIDE"];
const topArticleFooter = ["div", "top-article-footer", "FOOTER"];

const botArticle = ["div", "bot-article", "ARTICAL"];
const botArticleHeader = ["div", "bot-article-header", "HEADER"];
const botArticleFirstPar = ["div", "bot-article-main-par", "P"];
const botArticleNextPar = ["div", "bot-article-sub-par", "P"];
const botArticleFooter = ["div", "bot-article-footer", "FOOTER"];

const leftSectionFooter = ["div", "left-section-footer", "FOOTER"];

const mainFooter = ["div", "main-footer", "FOOTER"];
//УВАГА!! зараз підуть костилі...

// Поки ще нема...

//...
//основний Боді
body.classList.add("main-body");
body.setAttribute('id', 'bagua-table');
body.textContent = "BODY";

//верхній Хєдєр
body.appendChild(createTag(...topHeader));
    document.getElementsByClassName('main-head')[0]
    .appendChild(createTag(...topHeaderNav));

//центральна частина 
body.appendChild(createTag(...mainBlock));

    //ліва і права секція
    document.getElementsByClassName('main-block-middle')[0]
    .appendChild(createTag(...leftSection));
        //ліва секція 
        //Хедр лівої секції
        document.getElementsByClassName('left-section')[0]
        .appendChild(createTag(...leftSectionMainHeader));
        //Перший Арткль лівої секції
        document.getElementsByClassName('left-section')[0]
        .appendChild(createTag(...topArticle));
            //Наповнення першого Арткля лівої секції
            document.getElementsByClassName('top-article')[0]
            .appendChild(createTag(...topArticleHeader));
            document.getElementsByClassName('top-article')[0]
            .appendChild(createTag(...topArticleMainPar));
                //Додатковий Дів
            document.getElementsByClassName('top-article')[0]
            .appendChild(createTag(...topArticleSub));
                //2 горизонтальні кнопки
                document.getElementsByClassName('top-article-sub')[0]
                .appendChild(createTag(...topArticleSubPar));
                document.getElementsByClassName('top-article-sub')[0]
                .appendChild(createTag(...topArticleSubAside));
            document.getElementsByClassName('top-article')[0]
            .appendChild(createTag(...topArticleFooter));
        //Другий Арткль лівої секції
        document.getElementsByClassName('left-section')[0]
        .appendChild(createTag(...botArticle));
            //Наповнення другого Арткля лівої секції
            document.getElementsByClassName('bot-article')[0]
            .appendChild(createTag(...botArticleHeader));
            document.getElementsByClassName('bot-article')[0]
            .appendChild(createTag(...botArticleFirstPar));
            document.getElementsByClassName('bot-article')[0]
            .appendChild(createTag(...botArticleNextPar));
            document.getElementsByClassName('bot-article')[0]
            .appendChild(createTag(...botArticleFooter));   
        //Футр лівої сторони
        document.getElementsByClassName('left-section')[0]
        .appendChild(createTag(...leftSectionFooter));
    //права секція 
    document.getElementsByClassName('main-block-middle')[0]
    .appendChild(createTag(...rightSection));
        //елементи правої секції
        document.getElementsByClassName('right-section')[0]
        .appendChild(createTag(...righSectionHeader));
        document.getElementsByClassName('right-section')[0]
        .appendChild(createTag(...righSectionNav));

//Футр сторінки
body.appendChild(createTag(...mainFooter));

document.addEventListener('click',e => console.log(e.target))


////////////////////////////////////////////////
////////////////////////////////////////////////
let table = document.getElementById('bagua-table');

let editingTd;

table.onclick = function(event) {

  
  let target = event.target.closest('.edit-cancel,.edit-ok,div');

  if (!table.contains(target)) return;

  if (target.className == 'edit-cancel') {
    finishTdEdit(editingTd.elem, false);
  } else if (target.className == 'edit-ok') {
    finishTdEdit(editingTd.elem, true);
  } else if (target.nodeName == 'DIV') {
    if (editingTd) return; // уже редактируется

    makeTdEditable(target);
  }

};

function makeTdEditable(div) {
  editingTd = {
    elem: div,
    data: div.innerHTML
  };

  div.classList.add('edit-td'); // td в состоянии редактирования, CSS применятся к textarea внутри ячейки

  let textArea = document.createElement('textarea');
  textArea.style.width = div.clientWidth + 'px';
  textArea.style.height = div.clientHeight + 'px';
  textArea.className = 'edit-area';

  textArea.value = div.innerHTML;
  div.innerHTML = '';
  div.appendChild(textArea);
  textArea.focus();

  div.insertAdjacentHTML("beforeEnd",
    '<div class="edit-controls"><button class="edit-ok">OK</button><button class="edit-cancel">CANCEL</button></div>'
  );
}

function finishTdEdit(div, isOk) {
  if (isOk) {
    div.innerHTML = div.firstChild.value;
  } else {
    div.innerHTML = editingTd.data;
  }
  div.classList.remove('edit-td');
  editingTd = null;
}